from django.db import models
from django.utils import timezone

class Status(models.Model):
    status = models.TextField(max_length = 300)
    time_created = models.DateTimeField(default = timezone.now)