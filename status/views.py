from django.shortcuts import render
from status.forms import StatusForm
from status.models import Status

def index(request):
    form = StatusForm(request.POST or None)
    data = Status.objects.all()
    context = {
        'status' : data,
        'form'  : form,
    }
    if request.method == "POST" and form.is_valid():
        form.save()
    return render(request, 'status.html', context)