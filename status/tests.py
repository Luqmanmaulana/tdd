from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from status.views import index
from status.models import Status
from status.forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest


class StatusTest(TestCase):

    def test_status_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_status_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'status.html')
    
    def test_status_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):

        new_status = Status.objects.create(status="Aku lagi sedih")

        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)
    
    def test_status_form_valid(self):
        form_data = {
            "status" : "Aku sedih gembira"
        }
        status_form = StatusForm(data = form_data)
        self.assertTrue(status_form.is_valid())

    def test_post_status(self):
        response = self.client.post('/', {
            'status': "Aku sedih gembira",
        })
        self.assertIn("Aku sedih gembira", response.content.decode())

class NewVisitorTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_input_status(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Status', self.browser.page_source)

        status = self.browser.find_element_by_id('status')
        submit = self.browser.find_element_by_id('submit-status')
        status.send_keys('Functional test yuk')
        submit.send_keys(Keys.RETURN)
        self.assertIn('Functional test yuk', self.browser.page_source)