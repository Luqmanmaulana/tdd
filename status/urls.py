from django.contrib import admin
from django.urls import path
from status.views import index

app_name = "status"

urlpatterns = [
    path('', index, name='home'),
]