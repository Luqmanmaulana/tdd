from django import forms
from status.models import Status

class StatusForm(forms.ModelForm):
    status = forms.CharField(widget=forms.Textarea(
        attrs = {
            'type' : 'text',
            'id'   : 'status',
                }
    ))

    class Meta:
        model = Status
        fields = "__all__"
        exclude = (
            "time_created",
        )