from django.test import TestCase
from django.urls import resolve
from home.views import landing

class HomeTest(TestCase):

    def test_home_url_is_exist(self):
        response = self.client.get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_home_template(self):
        response = self.client.get('/home/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_home_using_landing_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, landing)
