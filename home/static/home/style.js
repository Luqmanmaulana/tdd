$(document).ready(function () {
    $('#customSwitch1').change(function(){
        var desc = $("#home-desc-tema")
        var navbar = $("#navbar-bootstrap")
        $(".panel").toggleClass('panel-dark')
        if($(this).is(':checked')){
            desc.text("GELAP");
            navbar.addClass('navbar-dark');
            navbar.addClass('bg-dark');
            navbar.removeClass('navbar-light');
            navbar.removeClass('bg-light');
            $('body').removeClass('light');
            $('body').addClass('dark');
        }
        else{
            desc.text("TERANG");
            navbar.addClass('navbar-light');
            navbar.addClass('bg-light');
            navbar.removeClass('navbar-dark');
            navbar.removeClass('bg-dark');
            $('body').removeClass('dark');
            $('body').addClass('light');
        }
    });

    var acc = $(".accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
        panel.style.display = "none";
        } else {
        panel.style.display = "block";
        }
    });
    } 
});