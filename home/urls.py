from django.contrib import admin
from django.urls import path
from home.views import landing

app_name = "home"

urlpatterns = [ 
    path('', landing, name='land'),
]