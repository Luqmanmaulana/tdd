from django.test import TestCase
from django.urls import resolve
from akun.views import index


class AkunTest(TestCase):

    def test_akun_url_is_exist(self):
        response = self.client.get('/akun/')
        self.assertEqual(response.status_code, 200)

    def test_akun_using_akun_template(self):
        response = self.client.get('/akun/')
        self.assertTemplateUsed(response, 'akun/akun.html')
    
    def test_akun_using_index_func(self):
        found = resolve('/akun/')
        self.assertEqual(found.func, index)
    
    def test_login_url_is_exist(self):
       response = self.client.get('/accounts/login/')
       self.assertEqual(response.status_code,200)

    def test_akun_using_login_template(self):
        response = self.client.get('/accounts/login/')
        self.assertTemplateUsed(response,'registration/login.html')
