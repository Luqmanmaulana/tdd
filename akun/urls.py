from django.contrib import admin
from django.urls import path
from akun.views import index

app_name = "akun"

urlpatterns = [
    path('', index, name='akun'),
]