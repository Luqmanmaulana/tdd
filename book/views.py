from django.shortcuts import render
from django.http.response import JsonResponse

import requests

def index(request):
    return render (request, "book.html")

def find_book(request, q):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q)
    books = response.json()
    return JsonResponse(books)