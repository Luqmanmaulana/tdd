from django.contrib import admin
from django.urls import path
from book.views import index, find_book

app_name = "book"

urlpatterns = [ 
    path('', index, name='index'),
    path('find_book/<str:q>', find_book),
]