$(document).ready(function (){
    // default
    search("random");
    // search
    $("#search").click(function(){
        const searchVal = $("#search-book").val();
        search(searchVal);
    })
})

function search(key){
    $.ajax({
        url:"/book/find_book/" + key,
        type: "GET",
        dataType: "json",
        success: function(result){

            const countItem = result.totalItems;
            let found = "found " + countItem + " books<br>show 10 books" 
            $("#found-book").html(found);
            const books = result.items;
            let table = "";

            for (let i = 0; i < 10; i++) {
                const item = books[i].volumeInfo;
                table += "<tr>"
                // judul
                table += "<td><a href="+item.previewLink+">" +item.title+"</a></td>";

                // foto sampul
                if (item.imageLinks != undefined){
                    table += "<td><img src="+item.imageLinks.thumbnail+"></td>";
                }
                else{
                    table += "<td>Foto tidak tersedia</td>";
                }

                // authors
                if (item.authors != undefined) {
                    table += "<td>"
                    for (let j = 0; j < item.authors.length; j++) {
                        table += item.authors +"<br>";
                    }
                    table += "</td>";
                } else{
                    table += "<td>Anonim</td>";
                }

                // description
                if (item.description != undefined) {
                    table += "<td>"+item.description+"</td>";
                } else{
                    table += "<td>None</td>";
                }
                table+="</tr>"
            }
            $("#result-body").html(table);
        }
    })
}