from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from book.views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest

class BookTest(TestCase):

    def test_book_url_is_exist(self):
        response = self.client.get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_book_using_index_template(self):
        response = self.client.get('/book/')
        self.assertTemplateUsed(response, 'book.html')
    
    def test_book_using_index_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, index)

    def test_found_book(self):
        response = self.client.get('/book/find_book/tere')
        self.assertContains(response, "tere")

class BookFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_search_book(self):
        self.browser.get(self.live_server_url+"/book/")
        self.assertIn('Search', self.browser.page_source)

        book = self.browser.find_element_by_id('search-book')
        submit = self.browser.find_element_by_id('search')
        book.send_keys('tere')
        submit.send_keys(Keys.RETURN)
        WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, "#result-body > tr")))

        self.assertIn('tere', self.browser.page_source)