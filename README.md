[![pipeline status](https://gitlab.com/Luqmanmaulana/tdd/badges/master/pipeline.svg)](https://gitlab.com/Luqmanmaulana/tdd/commits/master)
[![coverage report](https://gitlab.com/Luqmanmaulana/tdd/badges/master/coverage.svg)](https://gitlab.com/Luqmanmaulana/tdd/commits/master)

## Story 6:
Belajar menggunakan TDD dan functional test

## Story 9:
username: superuser
password: supahusah

## Story 10:
High Availability with nginx in windows

kali ini saya akan sharing mengenai bagaimana konfigurasi nginx untuk localhost pada windows

* download nginx di internet
* ekstrak folder di direktori favorit anda
* buka cmd lalu arahkan ke direktori nginx anda dan tulis perintah
  ```
  start nginx
  ```
  nginx akan terjalankan
* ubah nginx.conf, tambahkan 

    ```
    upstream localhost {
        server 127.0.0.1:8000;
        server 127.0.0.1:9000;
    }

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            #root   html;
            #index  index.html index.htm;
            proxy_pass http://localhost;
        }
    ```
    di baris ke 35
* untuk melihat perbedaan, jalankan server django di port berbeda, misal port 8000 dan 9000
* buka localhost di website
* nginx akan mengarahkan request ke port 8000 dan 9000 secara bergantian